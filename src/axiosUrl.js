import axios from "axios";

const AxiosUrl = axios.create({
    baseURL: 'https://blockapp2021-default-rtdb.europe-west1.firebasedatabase.app'
})

export default AxiosUrl;