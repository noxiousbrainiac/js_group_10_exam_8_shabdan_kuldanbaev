import React, {useEffect, useState} from 'react';
import AxiosUrl from "../../axiosUrl";
import Quote from "../../components/Quote/Quote";
import {useHistory, useParams} from "react-router-dom";
import Categories from "../../components/Categories/Categories";

const AllQuotes = () => {
    const [quotes, setQuotes] = useState([]);
    const history = useHistory();
    const {id} = useParams();

    const getQuotes = async () => {
        const {data} = id
            ? await AxiosUrl.get(`/quotes.json?orderBy="category"&equalTo="${id}"`)
            : await AxiosUrl.get('/quotes.json');
        setQuotes(data);
    }

    const onDelete = async (quote) => {
        await AxiosUrl.delete(`/quotes/${quote}.json`);
        await getQuotes();
        history.push('/');
    }

    const onEdit = (quote) => {
        history.push(`/quotes/${quote}/edit`);
    }

    useEffect(() => {
        getQuotes();
    }, [id])

    return (
        <div className="d-flex justify-content-evenly">
            <Categories categories={quotes}/>
            <div className="d-flex flex-column justify-content-evenly">
                {quotes
                    ? Object.keys(quotes).map(quote => (
                        <Quote
                            key={quote}
                            quote={quotes[quote]}
                            onDelete={() => onDelete(quote)}
                            onEdit={() => onEdit(quote)}/>
                    )) : null}
            </div>
        </div>
    );
};

export default AllQuotes;