import React, {useState} from 'react';
import AxiosUrl from "../../axiosUrl";
import {useHistory} from "react-router-dom";

const AddQuote = () => {
    const [newQuote, setNewQuote] = useState({
        category: "others",
        author: "",
        text: ""
    });

    const history = useHistory();

    const categories = [
        {category: "Star wars", id: "starWars"},
        {category: "Famous people", id: "famousPeople"},
        {category: "Saying", id: "saying"},
        {category: "Humour", id: "humour"},
        {category: "Motivational", id: "motivational"}
    ]

    const submitQuote = async e => {
        e.preventDefault();

        const quote = {
            category: newQuote.category,
            author: newQuote.author,
            text: newQuote.text
        }

        await AxiosUrl.post('/quotes.json', quote);
        history.push('/');
    }

    const onInputChange = e => {
        const {name, value} = e.target;
        setNewQuote(prev => ({
            ...prev,
            [name]: value,
        }));
    }

    return (
        <div className="container">
            <h3>Submit new quote</h3>
            <form
                onSubmit={newQuote.text.length > 0 && newQuote.author.length > 0 && newQuote.category.length > 0 ? submitQuote : null}
            >
                <div className="input-group my-2">
                    <div className="input-group-prepend">
                        <label className="input-group-text">Categories</label>
                    </div>
                    <select
                        className="custom-select"
                        onChange={onInputChange}
                        name="category"
                        value={newQuote.category}
                    >
                        <option value="Others" selected>Choose...</option>
                        {categories.map(cat => (
                            <option
                                key={cat.id}
                                value={cat.category}
                            >
                                {cat.category}
                            </option>
                        ))}
                    </select>
                </div>

                <div className="form-group my-2">
                    <label>Author</label>
                    <input
                        name="author"
                        onChange={onInputChange}
                        value={newQuote.author}
                        className="form-control"
                        placeholder="Enter author"
                    />
                </div>
                <div className="form-group my-2">
                    <label>Quote text</label>
                    <textarea
                        style={{resize: "none", height: "150px"}}
                        name="text"
                        value={newQuote.text}
                        onChange={onInputChange}
                        className="form-control"
                        placeholder="Enter quote"
                    />
                </div>
                <div className="form-group">
                    <button
                        className="btn btn-primary"
                    >
                        Save
                    </button>
                </div>
            </form>
        </div>
    );
};

export default AddQuote;