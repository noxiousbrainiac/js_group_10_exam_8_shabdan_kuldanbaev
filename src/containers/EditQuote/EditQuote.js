import React, {useEffect, useState} from 'react';
import AxiosUrl from "../../axiosUrl";
import {useHistory, useParams} from "react-router-dom";

const EditQuote = () => {
    const categories = [
        {category: "Star wars", id: "starWars"},
        {category: "Famous people", id: "famousPeople"},
        {category: "Saying", id: "saying"},
        {category: "Humour", id: "humour"},
        {category: "Motivational", id: "motivational"}
    ]

    const {id} = useParams();
    const history = useHistory();
    const [quote, setQuote] = useState({});

    const getQuote = async () => {
        const {data} = await AxiosUrl(`/quotes/${id}.json`);
        setQuote(data);
    }

    useEffect(() => {
        getQuote();
    }, [])

    const putQuote = async e => {
        e.preventDefault();
        await AxiosUrl.put('/quotes/' + id + '.json', quote);
        history.push('/');
    }

    const onInputChange = e => {
        const {name, value} = e.target;
        setQuote(prev => ({
            ...prev,
            [name]: value,
        }));
    }

    return (
        <>
            <div className="container">
                <h3>Submit new quote</h3>
                <form
                    onSubmit={putQuote}
                >
                    <div className="input-group my-2">
                        <div className="input-group-prepend">
                            <label className="input-group-text">Categories</label>
                        </div>
                        <select
                            className="custom-select"
                            onChange={onInputChange}
                            name="category"
                            value={quote.category}
                        >
                            <option value="selected" disabled>Choose...</option>
                            {categories.map(cat => (
                                <option
                                    key={cat.id}
                                    value={cat.category}
                                >
                                    {cat.category}
                                </option>
                            ))}
                        </select>
                    </div>

                    <div className="form-group my-2">
                        <label>Author</label>
                        <input
                            name="author"
                            onChange={onInputChange}
                            value={quote.author}
                            className="form-control"
                            placeholder="Enter author"
                        />
                    </div>
                    <div className="form-group my-2">
                        <label>Quote text</label>
                        <textarea
                            style={{resize: "none", height: "150px"}}
                            name="text"
                            value={quote.text}
                            onChange={onInputChange}
                            className="form-control"
                            placeholder="Enter quote"
                        />
                    </div>
                    <div className="form-group">
                        <button
                            className="btn btn-primary"
                        >
                            Save
                        </button>
                    </div>
                </form>
            </div>
        </>
    );
};

export default EditQuote;