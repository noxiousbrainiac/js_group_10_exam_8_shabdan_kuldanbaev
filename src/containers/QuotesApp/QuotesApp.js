import React from 'react';
import Navigation from "../../components/Navigation/Navigation";
import AddQuote from "../AddQuote/AddQuote";
import {Route, Switch} from "react-router-dom";
import HomePage from "../HomePage/HomePage";
import AllQuotes from "../AllQuotes/AllQuotes";
import EditQuote from "../EditQuote/EditQuote";

const QuotesApp = () => {
    return (
        <>
         <Navigation/>
            <div className="container">
                <Switch>
                    <Route path="/" exact component={HomePage}/>
                    <Route path="/addquote" component={AddQuote}/>
                    <Route path="/quotes/:id" exact component={AllQuotes}/>
                    <Route path="/quotes/:id/edit" component={EditQuote}/>
                </Switch>
            </div>
        </>
    );
};

export default QuotesApp;