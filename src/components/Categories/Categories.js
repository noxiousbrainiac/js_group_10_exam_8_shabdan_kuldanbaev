import React  from 'react';
import {Link} from "react-router-dom";

const Categories = () => {
    const categories = [
        {category: "Star wars", id: "starWars"},
        {category: "Famous people", id: "famousPeople"},
        {category: "Saying", id: "saying"},
        {category: "Humour", id: "humour"},
        {category: "Motivational", id: "motivational"}
    ]

    return (
        <ul style={{listStyle: "none"}} className="text-capitalize">
            <li><Link to="/">All</Link></li>
            {Object.keys(categories).map(cat => (
                <li key={cat}>
                    <Link
                        to={`/quotes/${categories[cat].category}`}
                    >
                        {categories[cat].category}
                    </Link>
                </li>
            ))}
        </ul>
    );
};

export default Categories;