import React from 'react';
import {Link} from "react-router-dom";

const Navigation = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light d-flex justify-content-between">
            <div className="container">
                <h1>Quotes</h1>
                <div className="navbar-nav">
                    <Link className="nav-item nav-link" to="/">Quotes</Link>
                    <Link className="nav-item nav-link" to="/addquote">Submit new quote</Link>
                </div>
            </div>
        </nav>
    );
};

export default Navigation;