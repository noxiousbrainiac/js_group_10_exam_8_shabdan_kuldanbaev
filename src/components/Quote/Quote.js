import React from 'react';

const Quote = ({quote ,onDelete, onEdit}) => {

    return (
        <div className="d-flex card my-2 p-2" style={{width: '500px'}}>
            <div>
                <p className="fst-italic">"{quote.text}"</p>
                <span>- {quote.author}</span>
            </div>
            <div className="d-flex justify-content-evenly">
                <button
                    className="btn btn-danger"
                    onClick={onDelete}
                >
                    Remove
                </button>
                <button
                    className="btn btn-warning"
                    onClick={onEdit}
                >
                    Edit
                </button>
            </div>
        </div>
    );
};

export default Quote;