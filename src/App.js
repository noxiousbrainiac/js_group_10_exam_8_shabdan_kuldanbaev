import React from 'react';
import QuotesApp from "./containers/QuotesApp/QuotesApp";

const App = () => {
    return (
        <div>
            <QuotesApp/>
        </div>
    );
};

export default App;